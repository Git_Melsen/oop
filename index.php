<?php
require('animal.php');
require('frog.php');
require('ape.php');

$animal = new Animal("Animal");
echo $animal->name ."<br>";
echo $animal->legs . "<br>";
var_dump($animal->sheep) . "<br>";
echo "<br>";
echo "<br>";

$frog = new Frog("Frog");
echo $frog->legs . "<br>";
echo $frog->jump();
echo "<br>";
echo "<br>";

$ape = new Ape("Ape");
echo $ape->legs . "<br>";
echo $ape->yell();;