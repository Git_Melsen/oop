<?php
	class Animal
	{
		public $name;
		public $legs = 2;
		public $sheep = false;

		public function __construct($name)
		{
			$this->name = $name;
		}
	}